import React from 'react';
import PropTypes from 'prop-types';

const ToolbarGrouping = (props) => {
  const { children } = props;

  return (
    <div className='toolbar__grouping'>
      <div className='toolbar__grouping-title'>
        Group by
      </div>
      <div className='toolbar__grouping-content'>
        {children}
      </div>
    </div>
  );
};

ToolbarGrouping.propTypes = {
  children: PropTypes.node,
};

export default ToolbarGrouping;
