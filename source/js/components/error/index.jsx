import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const Error = (props) => {
  const { children } = props;

  return (
    <div className='error'>
      {children}
    </div>
  );
};

Error.propTypes = {
  children: PropTypes.node,
};

export default Error;
