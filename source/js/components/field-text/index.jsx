import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Input from './input';
import './style.scss';

class FieldText extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocus: false,
      value: this.props.value,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value && nextProps.value !== undefined) {
      this.setState({ value: nextProps.value });
    }
  }

  handleOnFocus = () => {
    this.setState({ isFocus: true });
  };

  handleOnChange = (value) => {
    this.setState({ value });
    if (this.props.onChange) this.props.onChange({ fieldId: this.props.id, fieldValue: value });
  };

  handleOnBlur = () => {
    this.setState({ isFocus: false });
  };

  render() {
    const { id, placeholder } = this.props;
    const { isFocus, value } = this.state;

    const elementClassName = classnames('field-text', isFocus && 's-focused');

    return (
      <div className={ elementClassName }>
        <Input
          id={ id }
          value={ value }
          onFocus={ this.handleOnFocus }
          onChange={ this.handleOnChange }
          onBlur={ this.handleOnBlur }
          placeholder={ placeholder }
        />
      </div>
    );
  }
}

export default FieldText;
