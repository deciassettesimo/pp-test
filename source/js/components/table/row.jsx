import React from 'react';
import PropTypes from 'prop-types';

const TableRow = (props) => {
  const { children } = props;

  return (
    <div className='table__row'>
      {children}
    </div>
  );
};

TableRow.propTypes = {
  children: PropTypes.node,
};

export default TableRow;
