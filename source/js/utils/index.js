export const csvToArray = csv => {
  const lines = csv.split('\n');
  const result = [];
  const headers = lines[0].split(',');
  lines.forEach((line, lineIndex) => {
    if (lineIndex) {
      const obj = {};
      const data = line.split('","');
      headers.forEach((header, headerIndex) => {
        obj[header] = data[headerIndex].replace(/(^")|("$)/ig, '');
      });
      result.push(obj);
    }
  });
  return result;
};

export const widthMap = {
  '1/2': '50%',
  '1/3': '33.33%',
  '2/3': '66.66%',
  '1/4': '25%',
  '2/4': '50%',
  '3/4': '75%',
  '1/6': '16.66%',
  '2/6': '33.33%',
  '3/6': '50%',
  '4/6': '66.66%',
  '5/6': '83.33%',
};

export const regExpEscape = text => (text ? text.replace(/[-[\]{}()*+?.,\\^$|#]/gi, '\\$&') : '');

export * from './autos';
