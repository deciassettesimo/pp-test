import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const Table = (props) => {
  const { children } = props;

  return (
    <div className='table'>
      {children}
    </div>
  );
};

Table.propTypes = {
  children: PropTypes.node,
};

export default Table;

export TableGroup from './group';
export TableItem from './item';
export TableRow from './row';
export TableCell from './cell';
