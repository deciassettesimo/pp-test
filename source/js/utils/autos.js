import { fromJS } from 'immutable';
import { autosMarks, autosTypes } from 'config/autos';

export const normalizeAutosData = data => {
  const attributes = data.get('attributes').map(item => fromJS({
    id: item.get('ID'),
    title: item.get('Description'),
  }));

  const countries = data.get('countries').map(item => fromJS({
    id: item.get('ISO'),
    title: item.get('Description'),
  }));

  const colors = data.get('colors').map(item => {
    const attribute = attributes.find(a => a.get('id') === item.get('ID'));
    return fromJS({
      id: item.get('ID'),
      autos: item.get('Attribute').split(','),
      title: (attribute && attribute.get('title')) || null,
    });
  });

  const options = data.get('options').map(item => {
    const attribute = attributes.find(a => a.get('id') === item.get('Attribute'));
    return fromJS({
      id: item.get('Attribute'),
      autos: item.get('ID').split(','),
      title: (attribute && attribute.get('title')) || null,
    });
  });

  const autos = data.get('autos').map(item => {
    const id = item.get('ID');
    let description = item.get('Description');
    const country = countries.find(i => description.search(new RegExp(`${ i.get('id') }$`, 'i')) >= 0);
    const countryId = (country && country.get('id')) || null;
    description = description.replace(new RegExp(`\\s+${ countryId || '' }$`), '');
    const yearMatch = description.match(/\d{4}$/);
    const year = yearMatch[yearMatch.length - 1];
    description = description.replace(new RegExp(`\\s+${ year || '' }$`), '');
    const type = autosTypes.find(i => description.search(new RegExp(`${ i }$`, 'i')) >= 0);
    description = description.replace(new RegExp(`\\s+${ type || '' }$`), '');
    const mark = autosMarks.find(i => description.search(new RegExp(`^${ i }`, 'i')) >= 0);
    const model = description.replace(new RegExp(`^${ mark || '' }\\s+`), '');

    return fromJS({
      id,
      colors: colors.filter(i => i.get('autos').includes(id)).map(i => i.get('id')),
      country: countryId,
      mark: mark || null,
      model: model || null,
      options: options.filter(i => i.get('autos').includes(id)).map(i => i.get('id')),
      type: type || null,
      year: year ? parseInt(year, 10) : null,
    });
  });

  return fromJS({
    autos,
    colors: colors.map(item => item.delete('autos')),
    countries,
    marks: autosMarks,
    options: options.map(item => item.delete('autos')),
    types: autosTypes,
  });
};

export const getDictionary = (param) => {
  switch (param) {
    case 'mark':
      return 'marks';
    case 'type':
      return 'types';
    default:
      return null;
  }
};
