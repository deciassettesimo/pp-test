import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const Toolbar = (props) => {
  const { children } = props;

  return (
    <div className='toolbar'>
      {children}
    </div>
  );
};

Toolbar.propTypes = {
  children: PropTypes.node,
};

export default Toolbar;

export ToolbarGrouping from './grouping';
export ToolbarGroupingItem from './grouping-item';
export ToolbarSorting from './sorting';
export ToolbarSortingItem from './sorting-item';
export ToolbarSearching from './searching';
