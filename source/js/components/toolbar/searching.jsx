import React from 'react';
import PropTypes from 'prop-types';
import FieldText from 'components/field-text';

const ToolbarSearching = (props) => {
  const { placeholder, value, onChange } = props;

  return (
    <div className='toolbar__searching'>
      <FieldText
        id='ToolbarSearchingInput'
        placeholder={ placeholder }
        value={ value }
        onChange={ onChange }
      />
    </div>
  );
};

ToolbarSearching.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

export default ToolbarSearching;
