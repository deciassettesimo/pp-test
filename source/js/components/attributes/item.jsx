import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const AttributesItem = (props) => {
  const { label, value, isInline } = props;

  const elementClassName = classnames(
    'attributes__item',
    isInline && 'm-inline'
  );

  return (
    <div className={ elementClassName }>
      <div className='attributes__item-label'>
        {label}
        {isInline && ':'}
      </div>
      <div className='attributes__item-value'>
        {value}
      </div>
    </div>
  );
};

AttributesItem.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  isInline: PropTypes.bool,
};

export default AttributesItem;
