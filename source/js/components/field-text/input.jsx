import React, { Component } from 'react';
import PropTypes from 'prop-types';

class RboFormFieldTextInput extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
    onBlur: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.state = { value: props.value || '' };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState({ value: nextProps.value || '' });
    }
  }

  onFocus = () => {
    this.props.onFocus(this.state.value);
  };

  onChange = (e) => {
    const { value } = e.target;
    this.setState({ value });
    this.props.onChange(value, value);
  };

  onBlur = () => {
    this.props.onBlur(this.state.value);
  };

  render() {
    const { id, placeholder } = this.props;
    const { value } = this.state;

    return (
      <input
        className='field-text__input'
        spellCheck='false'
        autoComplete='off'
        autoCorrect='off'
        type='text'
        id={ id }
        value={ value }
        onFocus={ this.onFocus }
        onChange={ this.onChange }
        onBlur={ this.onBlur }
        placeholder={ placeholder }
      />
    );
  }
}

export default RboFormFieldTextInput;
