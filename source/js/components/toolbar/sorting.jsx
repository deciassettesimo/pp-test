import React from 'react';
import PropTypes from 'prop-types';

const ToolbarSorting = (props) => {
  const { children } = props;

  return (
    <div className='toolbar__sorting'>
      <div className='toolbar__sorting-title'>
        Sort by
      </div>
      <div className='toolbar__sorting-content'>
        {children}
      </div>
    </div>
  );
};

ToolbarSorting.propTypes = {
  children: PropTypes.node,
};

export default ToolbarSorting;
