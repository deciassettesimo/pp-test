# Project Plato Test 

## Get Started
1. **Install [Node ](https://nodejs.org)**.
2. **Clone this repository.** - `git clone https://deciassettesimo@bitbucket.org/deciassettesimo/pp-test.git`
3. **Make sure you're in the directory you just created.** - `cd pp-test`
4. **Install Node Packages.** - `npm install`
5. **Run the app.** - `npm start`
