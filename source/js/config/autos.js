export const autosMarks = [
  'AMC',
  'Audi',
  'Bmw',
  'Buick',
  'Chevrolet',
  'Datsun',
  'Dodge',
  'Ford',
  'Honda',
  'Mazda',
  'Mercedes benz',
  'Mercury',
  'Oldsmobile',
  'Opel',
  'Plymouth',
  'Pontiac',
  'Volvo',
];

export const autosTypes = [
  'Cabrio',
  'Coupe',
  'Pickup',
  'Sedan',
  'Station wagons',
];
