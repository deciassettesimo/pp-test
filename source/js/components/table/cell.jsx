import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { widthMap } from 'utils';

const TableCell = (props) => {
  const { children, align, width } = props;

  const elementClassName = classnames(
    'table__cell',
    align && `m-align-${ align }`
  );
  const style = { width: widthMap[width] || width || '100%' };

  return (
    <div className={ elementClassName } style={ style }>
      {children}
    </div>
  );
};

TableCell.propTypes = {
  children: PropTypes.node,
  align: PropTypes.oneOf(['left', 'center', 'right']),
  width: PropTypes.string,
};

export default TableCell;
