import React from 'react';
import PropTypes from 'prop-types';

const TableGroup = (props) => {
  const { children, title } = props;

  return (
    <div className='table__group'>
      {title &&
      <div className='table__group-title'>
        {title}
      </div>
      }
      <div className='table__group-content'>
        {children}
      </div>
    </div>
  );
};

TableGroup.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node,
};

export default TableGroup;
