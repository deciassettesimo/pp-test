import React from 'react';
import PropTypes from 'prop-types';

const SimilarItem = (props) => {
  const { value } = props;

  return (
    <div className='similar__item'>
      <div className='similar__item-icon' />
      <div className='similar__item-title'>
        {value}
      </div>
    </div>
  );
};

SimilarItem.propTypes = {
  value: PropTypes.string,
};

export default SimilarItem;
