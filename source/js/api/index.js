import promisePolyfill from 'es6-promise';
import 'isomorphic-fetch';
import { csvToArray } from 'utils';

promisePolyfill.polyfill();

function loadDataAttributes() {
  return fetch('/data-input/data-attributes.csv')
    .then(response => response.text())
    .then(text => csvToArray(text));
}

function loadDataAuto() {
  return fetch('/data-input/data-auto.csv')
    .then(response => response.text())
    .then(text => csvToArray(text));
}

function loadDataColors() {
  return fetch('/data-input/data-colors.csv')
    .then(response => response.text())
    .then(text => csvToArray(text));
}

function loadDataCountries() {
  return fetch('/data-input/data-countries.csv')
    .then(response => response.text())
    .then(text => csvToArray(text));
}

function loadDataOptions() {
  return fetch('/data-input/data-options.csv')
    .then(response => response.text())
    .then(text => csvToArray(text));
}

export default {
  loadDataAttributes,
  loadDataAuto,
  loadDataColors,
  loadDataCountries,
  loadDataOptions,
};
