import { combineReducers } from 'redux-immutable';
import { REDUCER as AUTOS_REDUCER } from 'constants/autos';
import autos from 'reducers/autos';

export default combineReducers({
  [AUTOS_REDUCER]: autos,
});
