import { fromJS } from 'immutable';
import { ACTIONS } from 'constants/autos';
import { normalizeAutosData } from 'utils';

const initialState = fromJS({
  idDataLoading: true,
  error: null,
  data: {},
});

const loadDataRequest = state => state.merge({
  idDataLoading: true,
  error: null,
  data: {},
  grouped: null,
  sorted: 'mark',
  searching: null,
  isActive: null,
});

const loadDataSuccess = (state, payload) => state.merge({
  idDataLoading: false,
  data: normalizeAutosData(fromJS(payload)),
});

const loadDataFail = (state, error) => state.merge({
  idDataLoading: false,
  error,
});

const toggleGrouping = (state, { groupingId }) => state.merge({
  grouped: state.get('grouped') === groupingId ? null : groupingId,
});

const toggleSorting = (state, { sortingId }) => state.merge({
  sorted: sortingId,
});

const changeSearching = (state, { value }) => state.merge({
  searching: value,
});

const toggleActiveItem = (state, { itemId }) => state.merge({
  isActive: itemId,
});

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case ACTIONS.AUTOS_LOAD_DATA_REQUEST:
      return loadDataRequest(state);
    case ACTIONS.AUTOS_LOAD_DATA_SUCCESS:
      return loadDataSuccess(state, action.payload);
    case ACTIONS.AUTOS_LOAD_DATA_FAIL:
      return loadDataFail(state, action.error);
    case ACTIONS.AUTOS_TOGGLE_GROUPING:
      return toggleGrouping(state, action.payload);
    case ACTIONS.AUTOS_TOGGLE_SORTING:
      return toggleSorting(state, action.payload);
    case ACTIONS.AUTOS_CHANGE_SEARCHING:
      return changeSearching(state, action.payload);
    case ACTIONS.AUTOS_TOGGLE_ACTIVE_ITEM:
      return toggleActiveItem(state, action.payload);
    default:
      return state;
  }
}
