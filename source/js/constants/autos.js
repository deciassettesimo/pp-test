import keyMirror from 'keymirror';

export const ACTIONS = keyMirror({
  AUTOS_LOAD_DATA_REQUEST: null,
  AUTOS_LOAD_DATA_SUCCESS: null,
  AUTOS_LOAD_DATA_FAIL: null,

  AUTOS_TOGGLE_GROUPING: null,
  AUTOS_TOGGLE_SORTING: null,
  AUTOS_CHANGE_SEARCHING: null,
  AUTOS_TOGGLE_ACTIVE_ITEM: null,
});

export const REDUCER = 'autos';
