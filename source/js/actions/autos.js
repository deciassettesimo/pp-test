import api from 'api';
import { ACTIONS } from 'constants/autos';

export function mount() {
  return function (dispatch) {
    dispatch({ type: ACTIONS.AUTOS_LOAD_DATA_REQUEST });

    Promise.all([
      api.loadDataAttributes(),
      api.loadDataAuto(),
      api.loadDataColors(),
      api.loadDataCountries(),
      api.loadDataOptions(),
    ])
      .then(data => dispatch({
        type: ACTIONS.AUTOS_LOAD_DATA_SUCCESS,
        payload: {
          attributes: data[0],
          autos: data[1],
          colors: data[2],
          countries: data[3],
          options: data[4],
        },
      }))
      .catch(error => dispatch({ type: ACTIONS.AUTOS_LOAD_DATA_FAIL, error }));
  };
}

export function toggleGrouping(payload) {
  return function (dispatch) {
    dispatch({ type: ACTIONS.AUTOS_TOGGLE_GROUPING, payload });
  };
}

export function toggleSorting(payload) {
  return function (dispatch) {
    dispatch({ type: ACTIONS.AUTOS_TOGGLE_SORTING, payload });
  };
}

export function changeSearching(payload) {
  return function (dispatch) {
    dispatch({ type: ACTIONS.AUTOS_CHANGE_SEARCHING, payload });
  };
}

export function toggleActiveItem(payload) {
  return function (dispatch) {
    dispatch({ type: ACTIONS.AUTOS_TOGGLE_ACTIVE_ITEM, payload });
  };
}
