import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const Similar = (props) => {
  const { children } = props;

  return (
    <div className='similar'>
      {children}
    </div>
  );
};

Similar.propTypes = {
  children: PropTypes.node,
};

export default Similar;

export SimilarItem from './item';
