import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Autos from 'views/Autos';

export default () => (
  <Switch>
    <Route exact path='/' component={ Autos } />
  </Switch>
);
