import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { mount, toggleGrouping, toggleSorting, changeSearching, toggleActiveItem } from 'actions/autos';
import mapStateToProps from 'selectors/autos';
import Loader from 'components/loader';
import Error from 'components/error';
import Toolbar, { ToolbarGrouping, ToolbarGroupingItem, ToolbarSorting, ToolbarSortingItem, ToolbarSearching } from 'components/toolbar';
import Table, { TableGroup, TableItem, TableRow, TableCell } from 'components/table';
import Attributes, { AttributesGroup, AttributesItem } from 'components/attributes';
import Similar, { SimilarItem } from 'components/similar';
import './style.scss';

@connect(state => mapStateToProps(state))

export default class Autos extends Component {
  static propTypes = {
    isDataLoading: PropTypes.bool,
    error: ImmutablePropTypes.map,
    list: ImmutablePropTypes.list,
    grouped: PropTypes.string,
    sorted: PropTypes.string,
    searching: PropTypes.string,
    dispatch: PropTypes.func,
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(mount());
  }

  handleGroupingClick = (groupingId) => {
    const { dispatch } = this.props;
    dispatch(toggleGrouping({ groupingId }));
  };

  handleSortingClick = (sortingId) => {
    const { dispatch } = this.props;
    dispatch(toggleSorting({ sortingId }));
  };

  handleSearchingChange = ({ fieldValue }) => {
    const { dispatch } = this.props;
    dispatch(changeSearching({ value: fieldValue }));
  };

  handleItemClick = (itemId) => {
    const { dispatch } = this.props;
    dispatch(toggleActiveItem({ itemId }));
  };

  render() {
    const {
      isDataLoading,
      error,
      list,
      grouped,
      sorted,
      searching,
    } = this.props;

    return (
      <div className='autos'>
        {isDataLoading && <Loader />}
        {error && <Error>Error: { error.get('message') }</Error>}
        {!isDataLoading && !error && list &&
        <Fragment>
          <Toolbar>
            <ToolbarGrouping>
              <ToolbarGroupingItem
                id='mark'
                title='Mark'
                isActive={ grouped === 'mark' }
                onClick={ this.handleGroupingClick }
              />
              <ToolbarGroupingItem
                id='type'
                title='Type'
                isActive={ grouped === 'type' }
                onClick={ this.handleGroupingClick }
              />
            </ToolbarGrouping>
            <ToolbarSorting>
              <ToolbarSortingItem
                id='mark'
                title='Mark'
                isActive={ sorted === 'mark' }
                onClick={ this.handleSortingClick }
              />
              <ToolbarSortingItem
                id='type'
                title='Type'
                isActive={ sorted === 'type' }
                onClick={ this.handleSortingClick }
              />
              <ToolbarSortingItem
                id='year'
                title='Year'
                isActive={ sorted === 'year' }
                onClick={ this.handleSortingClick }
              />
            </ToolbarSorting>
            <ToolbarSearching
              placeholder='Search'
              value={ searching }
              onChange={ this.handleSearchingChange }
            />
          </Toolbar>
          <Table>
            {list.map(group => (
              <TableGroup
                key={ group.get('id') }
                title={ group.get('title') }
              >
                {group.get('items').map(item => (
                  <TableItem
                    key={ item.get('id') }
                    id={ item.get('id') }
                    isActive={ item.get('isActive') }
                    onClick={ this.handleItemClick }
                  >
                    <TableRow>
                      <TableCell width='1/4'>{ item.get('mark') }</TableCell>
                      <TableCell width='1/4'>{ item.get('model') }</TableCell>
                      <TableCell width='1/4'>{ item.get('type') }</TableCell>
                      <TableCell width='1/4'>{ item.get('year') }</TableCell>
                    </TableRow>
                    {item.get('isActive') &&
                    <TableRow>
                      <TableCell>
                        <Attributes>
                          <AttributesGroup>
                            <AttributesItem
                              label='Year'
                              value={ item.get('year') }
                            />
                            <AttributesItem
                              label='Color'
                              value={ item.get('colors') }
                            />
                            <AttributesItem
                              label='Country'
                              value={ item.get('country') }
                            />
                          </AttributesGroup>
                          <AttributesGroup>
                            <AttributesItem
                              isInline
                              label='Options'
                              value={ item.get('options') }
                            />
                          </AttributesGroup>
                        </Attributes>
                        {!!item.get('similar').size &&
                        <Similar>
                          {item.get('similar').map(similar => (
                            <SimilarItem
                              key={ similar.get('id') }
                              value={ similar.get('title') }
                            />
                          ))}
                        </Similar>
                        }
                      </TableCell>
                    </TableRow>
                    }
                  </TableItem>
                ))}
              </TableGroup>
            ))}
          </Table>
        </Fragment>
        }
      </div>
    );
  }
}
