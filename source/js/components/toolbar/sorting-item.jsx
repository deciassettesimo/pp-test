import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const ToolbarSortingItem = (props) => {
  const { id, title, isActive, onClick } = props;

  const handleClick = () => {
    if (!isActive) onClick(id);
  };

  const elementClassName = classnames(
    'toolbar__sorting-item',
    isActive && 's-active'
  );

  return (
    <div className={ elementClassName } role='presentation' onClick={ handleClick }>
      {title}
    </div>
  );
};

ToolbarSortingItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  isActive: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
};

export default ToolbarSortingItem;
