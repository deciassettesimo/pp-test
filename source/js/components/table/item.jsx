import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const TableItem = (props) => {
  const { id, children, isActive, onClick } = props;

  const elementClassName = classnames(
    'table__item',
    isActive && 's-active'
  );

  const handleClick = () => {
    if (!isActive && onClick) onClick(id);
  };

  return (
    <div role='presentation' className={ elementClassName } onClick={ handleClick }>
      {children}
    </div>
  );
};

TableItem.propTypes = {
  id: PropTypes.string.isRequired,
  children: PropTypes.node,
  isActive: PropTypes.bool,
  onClick: PropTypes.func,
};

export default TableItem;
