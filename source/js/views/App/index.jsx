import React, { Component } from 'react';
import Routes from 'config/routes';
import './style.scss';

export default class App extends Component {
  render() {
    return (
      <div className='app'>
        <Routes />
      </div>
    );
  }
}
