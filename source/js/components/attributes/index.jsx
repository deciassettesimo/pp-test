import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const Attributes = (props) => {
  const { children } = props;

  return (
    <div className='attributes'>
      {children}
    </div>
  );
};

Attributes.propTypes = {
  children: PropTypes.node,
};

export default Attributes;

export AttributesGroup from './group';
export AttributesItem from './item';
