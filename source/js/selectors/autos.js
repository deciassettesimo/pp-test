import { createSelector } from 'reselect';
import { fromJS } from 'immutable';
import { REDUCER } from 'constants/autos';
import { getDictionary, regExpEscape } from 'utils';

const isDataLoadingSelector = state => state.getIn([REDUCER, 'isDataLoading']);
const errorSelector = state => state.getIn([REDUCER, 'error']);
const dataSelector = state => state.getIn([REDUCER, 'data']);
const groupedSelector = state => state.getIn([REDUCER, 'grouped']);
const sortedSelector = state => state.getIn([REDUCER, 'sorted']);
const searchingSelector = state => state.getIn([REDUCER, 'searching']);
const isActiveSelector = state => state.getIn([REDUCER, 'isActive']);

const listCreatedSelector = createSelector(
  dataSelector,
  groupedSelector,
  sortedSelector,
  searchingSelector,
  isActiveSelector,
  (data, grouped, sorted, searching, isActive) => {
    const list = data.get('autos');
    if (!list) return null;
    const formattedList = list
      .filter(item => {
        if (!searching) return true;
        const colors = data.get('colors')
          .filter(color => item.get('colors').includes(color.get('id')))
          .map(color => color.get('title'))
          .join(' ');
        const options = data.get('options')
          .filter(option => item.get('options').includes(option.get('id')))
          .map(option => option.get('title'))
          .join(' ');
        const country = data.get('countries').find(c => c.get('id') === item.get('country')).get('title');
        const searchString = `${ item.get('mark') } ${ item.get('model') } ${ item.get('type') } ${ item.get('year') } ${ colors } ${ options } ${ country }`;
        return searchString.search(new RegExp(regExpEscape(searching), 'ig')) >= 0;
      })
      .map(item => item.merge({
        isActive: item.get('id') === isActive,
        year: item.get('year').toString(),
        colors: item.get('colors').size
          ? data.get('colors')
            .filter(color => item.get('colors').includes(color.get('id')))
            .map(color => color.get('title'))
            .join(', ')
          : '—',
        country: item.get('country')
          ? data.get('countries').find(country => country.get('id') === item.get('country')).get('title')
          : '—',
        options: item.get('options').size
          ? data.get('options')
            .filter(option => item.get('options').includes(option.get('id')))
            .map(option => option.get('title'))
            .join(', ')
          : '—',
        similar: list
          .filter(similar => similar.get('id') !== item.get('id'))
          .filter(similar => {
            let conj = 0;
            if (similar.get('colors').filter(color => item.get('colors').includes(color)).size) conj++;
            if (similar.get('options').filter(option => item.get('options').includes(option)).size) conj++;
            if (similar.get('type') === item.get('type')) conj++;
            return conj >= 2;
          })
          .map(similar => fromJS({
            id: similar.get('id'),
            title: `${ similar.get('mark') } ${ similar.get('model') }`,
          })),
      }))
      .sort((a, b) => {
        switch (sorted) {
          case 'year':
            return a.get(sorted) > b.get(sorted) ? 1 : -1;
          default:
            return a.get(sorted).localeCompare(b.get(sorted));
        }
      });
    if (!grouped) {
      return fromJS([
        {
          id: 'notGrouped',
          items: formattedList,
        },
      ]);
    }
    const dictionary = getDictionary(grouped);
    return data.get(dictionary)
      .map(group => fromJS({
        id: group,
        title: group,
        items: formattedList.filter(item => item.get(grouped) === group),
      }))
      .filter(group => !!group.get('items').size);
  }
);

const mapStateToProps = state => ({
  isDataLoading: isDataLoadingSelector(state),
  error: errorSelector(state),
  list: listCreatedSelector(state),
  grouped: groupedSelector(state),
  sorted: sortedSelector(state),
  searching: searchingSelector(state),
});

export default mapStateToProps;
