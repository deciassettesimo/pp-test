import React from 'react';
import PropTypes from 'prop-types';

const AttributesGroup = (props) => {
  const { children } = props;

  return (
    <div className='attributes__group'>
      {children}
    </div>
  );
};

AttributesGroup.propTypes = {
  children: PropTypes.node,
};

export default AttributesGroup;
