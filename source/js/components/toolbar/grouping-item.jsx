import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const ToolbarGroupingItem = (props) => {
  const { id, title, isActive, onClick } = props;

  const handleClick = () => onClick(id);

  const elementClassName = classnames(
    'toolbar__grouping-item',
    isActive && 's-active'
  );

  return (
    <div className={ elementClassName } role='presentation' onClick={ handleClick }>
      {title}
    </div>
  );
};

ToolbarGroupingItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  isActive: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
};

export default ToolbarGroupingItem;
